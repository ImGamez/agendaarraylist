package com.example.cursoandroid.agendaarraylist;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    final ArrayList<Contacto> contactos = new ArrayList<>();
    private EditText edtNombre;
    private EditText edtTelefono;
    private EditText edtTelefono2;
    private EditText edtDireccion;
    private EditText edtNotas;
    private CheckBox cbxFavorito;
    private Contacto saveContact;
    private int savedIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombre = (EditText) findViewById(R.id.edtNombre);
        edtTelefono = (EditText) findViewById(R.id.edtTel1);
        edtTelefono2 = (EditText) findViewById(R.id.edtTel2);
        edtDireccion = (EditText) findViewById(R.id.edtDomicilio);
        edtNotas = (EditText) findViewById(R.id.edtNota);
        cbxFavorito = (CheckBox) findViewById(R.id.chkFavorito);
        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);
        Button btnCerrar = (Button) findViewById(R.id.btnCerrar);

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtNombre.getText().toString().matches("") || edtTelefono.getText().toString().matches("") ||
                        edtDireccion.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this, "Ingrese todos los datos", Toast.LENGTH_SHORT).show();
                } else {
                    Contacto nContacto = new Contacto();
                    int index = contactos.size();
                    if(saveContact != null) {
                        contactos.remove(savedIndex);
                        nContacto = saveContact;
                        index = savedIndex;
                    }
                    saveContact = new Contacto();
                    saveContact.setNombre(edtNombre.getText().toString());
                    saveContact.setDomicilio(edtDireccion.getText().toString());
                    saveContact.setNotas(edtNotas.getText().toString());
                    saveContact.setTelefono1(edtTelefono.getText().toString());
                    saveContact.setTelefono2(edtTelefono2.getText().toString());
                    saveContact.setFavorito(cbxFavorito.isChecked());
                    contactos.add(savedIndex, saveContact);
                    Toast.makeText(MainActivity.this, R.string.mensaje, Toast.LENGTH_SHORT).show();
                    saveContact = null;
                    limpiar();
                }
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, com.example.cursoandroid.agendaarraylist.ListActivity.class);
                Bundle bObject = new Bundle();
                bObject.putSerializable("contactos", contactos);
                i.putExtras(bObject);
                startActivityForResult(i, 0);
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(intent != null) {
            Bundle oBundle = intent.getExtras();

            saveContact = (Contacto) oBundle.getSerializable("contactos");
            savedIndex = oBundle.getInt("index");
            edtNombre.setText(saveContact.getNombre());
            edtTelefono.setText(saveContact.getTelefono1());
            edtTelefono2.setText(saveContact.getTelefono2());
            edtDireccion.setText(saveContact.getDomicilio());
            edtNotas.setText(saveContact.getNotas());
            cbxFavorito.setChecked(saveContact.isFavorito());
        } else {
            limpiar();
        }
    }


    public void limpiar() {
        edtNombre.setText("");
        edtDireccion.setText("");
        edtTelefono.setText("");
        edtTelefono2.setText("");
        edtNotas.setText("");
        cbxFavorito.setChecked(false);
        saveContact = null;
    }
}

package com.example.cursoandroid.agendaarraylist;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.SearchView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {
    private TableLayout tblLista;
    public static ArrayList<Contacto> contactos;
    ArrayList<Contacto> filter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        tblLista = (TableLayout) findViewById(R.id.tblLista);
        Bundle bundleObject = getIntent().getExtras();
        contactos = (ArrayList<Contacto>) bundleObject.getSerializable("contactos");
        this.filter = this.contactos;
        Button btnNuevo = (Button) findViewById(R.id.btnNuevo);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
        cargarContactos();
    }

    public void cargarContactos() {
        for(int x=0; x < contactos.size(); x++) {
            final Contacto c = contactos.get(x);
            TableRow nRow = new TableRow(ListActivity.this);

            TextView nText = new TextView(ListActivity.this);
            nText.setText(c.getNombre());

            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nText.setTextColor((c.isFavorito()) ? Color.BLUE : Color.BLACK);
            nRow.addView(nText);

            Button nButton = new Button(ListActivity.this);
            nButton.setText(R.string.accion);
            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nButton.setTextColor(Color.BLACK);

            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Contacto c= (Contacto) v.getTag(R.string.contacto_g);
                    Intent i = new Intent();
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contactos", c);
                    oBundle.putInt("index",Integer.valueOf(v.getTag(R.string.contacto_g_index).toString()));
                    i.putExtras(oBundle);
                    setResult(RESULT_OK, i);
                    finish();
                }
            });
            nButton.setTag(R.string.contacto_g, c);
            nButton.setTag(R.string.contacto_g_index,x);
            nRow.addView(nButton);

            Button btnElimiar = new Button(ListActivity.this);
            btnElimiar.setText(R.string.btnBorrar);
            btnElimiar.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            btnElimiar.setTextColor(Color.BLACK);

            btnElimiar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    long id = Long.parseLong(String.valueOf(v.getTag(R.string.contacto_g_index)));
                    elimianarContacto(id);
                }
            });
            btnElimiar.setTag(R.string.contacto_g_index, c.getID());
            nRow.addView(btnElimiar);

            tblLista.addView(nRow);
        }
    }

    private void elimianarContacto(long id)
    {
        for(int x = 0; x < filter.size(); x++)
        {
            if(filter.get(x).getID() == id)
            {
                filter.remove(x);
                break;
            }
        }
        contactos = filter;
        tblLista.removeAllViews();
        cargarContactos();

    }

    public void buscar(String s){
        ArrayList<Contacto> list = new ArrayList<>();
        for(int x = 0; x < filter.size(); x++)
        {
            if(filter.get(x).getNombre().contains(s))
                list.add(filter.get(x));
        }

        contactos = list;
        tblLista.removeAllViews();
        cargarContactos();
    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_search ,menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                buscar(s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

}
